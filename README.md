# README #

This README would normally document whatever steps are necessary to get your application up and running.

# NOTE : License #

* GNU GENERAL PUBLIC LICENSE
* I have added general public license and generated this license through github.
* I choosed this license as it is a free, copyleft license for software and other kinds of works.

# How to build install and use project #
* For using the project in your computer, first it is to be cloned in the folder.
* After cloning, you can open it in the visual code application.
* For running the project, first we need to install packages. 
* You can install package by writing npm install in the terminal of the visual code.
* Once it is installed, you can write node index.js and press enter.
* The project will get run in the web browser.
* Project is of buying things online.


### What is this repository for? ###

* Quick summary : This repository is for practicing the version control of the project through bitbucket and sourcetree.
* Version : 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up 
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact